
module.exports = {
  scripts: {
    default: 'electron-webpack dev',
    compile: 'electron-webpack',
    dist: {
      default: 'nps compile && electron-builder',
      dir: 'nps dist --dir -c.compression=store -c.mac.identity=null'
    }
  }
}

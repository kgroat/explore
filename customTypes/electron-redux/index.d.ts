
declare module 'electron-redux' {
  import { Middleware, Store } from 'redux'

  export interface MiddlewareOptions {
    blacklist?: RegExp[]
  }

  // For Main
  export const forwardToRenderer: Middleware
  export const triggerAlias: Middleware
  export function replayActionMain (store: Store): void

  // For Renderer
  export const forwardToMain: Middleware
  export function forwardToMainWithParams (opts: MiddlewareOptions): Middleware
  export function replayActionRenderer (store: Store): void
  export function getInitialStateRenderer<T = {}> (): T
}

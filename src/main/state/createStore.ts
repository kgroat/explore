// in the main store
import { createStore, applyMiddleware } from 'redux'
import { forwardToRenderer, replayActionMain } from 'electron-redux'

import { rootReducer } from '/common/state/rootRducer'

const store = createStore(
  rootReducer,
  {}, // optional
  applyMiddleware(
    // ...otherMiddleware,
    forwardToRenderer, // IMPORTANT! This goes last
  ),
)

replayActionMain(store)

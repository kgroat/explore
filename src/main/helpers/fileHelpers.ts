
import { getType } from 'mime'
import { readdir, readFile, stat } from 'fs'
import { dirname, basename } from 'path'
import { promisify } from 'util'

import { translateFileAccess, translateMode, Accessor } from '/common/types/FileAccess'
import { FileInfo } from '/common/types/FileInfo'
import { FileType } from '/common/types/FileType'

const readdirAsync = promisify(readdir)
const readFileAsync = promisify(readFile)
const statAsync = promisify(stat)

function isErrnoException (error: any): error is NodeJS.ErrnoException {
  return error.errno !== undefined
      || error.code !== undefined
      || error.path !== undefined
      || error.syscall !== undefined
}

export async function getTrueFileInfo (filename: string): Promise<FileInfo | null> {
  try {
    const fileStats = await statAsync(filename)
    const mode = translateMode(fileStats.mode)
    return {
      name: basename(filename),
      location: dirname(filename),
      type: fileStats.isDirectory() ? FileType.directory : FileType.file,
      mimeType: getType(filename) || 'text/plain',
      owner: fileStats.uid.toString(10),
      group: fileStats.gid.toString(10),
      mode,
      ownerAccess: translateFileAccess(mode[Accessor.owner]),
      groupAccess: translateFileAccess(mode[Accessor.group]),
      otherAccess: translateFileAccess(mode[Accessor.other]),
      created: fileStats.ctime,
      modified: fileStats.mtime,
      size: fileStats.size,
    }
  } catch (e) {
    if (isErrnoException(e)) {
      return null
    }

    throw e
  }
}

function mapFileInfo (original: FileInfo): FileInfo {
  if (process.platform !== 'darwin') {
    return original
  }

  return {
    ...original,
    type: original.type === FileType.directory && original.name.endsWith('.app')
        ? FileType.executable
        : original.type,
  }
}

export async function getFileInfo (filename: string): Promise<FileInfo | null> {
  const original = await getTrueFileInfo(filename)

  if (original) {
    return mapFileInfo(original)
  }

  return null
}

export async function getTrueDirectoryInfo (directory: string): Promise<FileInfo[]> {
  const files = await readdirAsync(directory)
  const fileInfos = await Promise.all(files.map(getTrueFileInfo))
  return fileInfos.filter(i => i) as FileInfo[]
}

export async function getDirectoryInfo (directory: string): Promise<FileInfo[]> {
  const originals = await getTrueDirectoryInfo(directory)
  return originals.map(mapFileInfo)
}

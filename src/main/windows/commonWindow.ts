'use strict'

import { BrowserWindow } from 'electron'
import { isDevelopment } from '../config'

export async function createWindow (): Promise<BrowserWindow> {
  const window = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },
  })

  if (isDevelopment) {
    window.webContents.openDevTools()
  }

  window.webContents.on('devtools-opened', () => {
    window.focus()
    setImmediate(() => {
      window.focus()
    })
  })

  return window
}

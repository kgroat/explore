'use strict'

import { app, BrowserWindow } from 'electron'
import * as path from 'path'
import { format as formatUrl } from 'url'

import { createWindow } from './commonWindow'
import { isDevelopment } from '../config'

// global reference to explorer windows (necessary to prevent window from being garbage collected)
const explorerWindows = new Set<BrowserWindow>()

export async function createExplorerWindow (): Promise<BrowserWindow> {
  const window = await createWindow()

  if (isDevelopment) {
    await window.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`)
  } else {
    await window.loadURL(formatUrl({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file',
      slashes: true,
    }))
  }

  window.on('closed', () => {
    explorerWindows.delete(window)
  })

  window.webContents.on('devtools-opened', () => {
    window.focus()
    setImmediate(() => {
      window.focus()
    })
  })

  return window
}

// quit application when all windows are closed
app.on('window-all-closed', () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

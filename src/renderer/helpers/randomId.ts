
const randomNumber = () => 1 + Math.random()
const digits = () => (randomNumber() * 0x10000 | 0).toString(16).substring(1)

export function getRandomId (): string {
  return `${digits()}${digits()}`
}

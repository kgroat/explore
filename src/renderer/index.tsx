
import * as React from 'react'
import { render } from 'react-dom'

import './global-css/index.pcss'
// import './global-css/fonts.pcss'

const App = () => {
  return <div>Hello, world</div>
}

render(<App />, document.getElementById('app'))

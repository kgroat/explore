// in the renderer store
import { createStore, applyMiddleware } from 'redux'
import { forwardToMain, replayActionRenderer, getInitialStateRenderer } from 'electron-redux'

import { rootReducer } from '/common/state/rootRducer'

const initialState = getInitialStateRenderer()

const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(
    forwardToMain, // IMPORTANT! This goes first
    // ...otherMiddleware,
  ),
)

replayActionRenderer(store)


export async function mapAsync<In, Out> (inputs: In[], mapper: (each: In) => Promise<Out>, limit = Infinity): Promise<Out[]> {
  if (limit > inputs.length) {
    return Promise.all(inputs.map(mapper))
  }

  let i = 0
  const outputs: Out[] = []

  async function scheduleNext () {
    if (i >= inputs.length) {
      return
    }

    const idx = i++
    const input = inputs[idx]
    outputs[idx] = await mapper(input)
    if (i < inputs.length) {
      await scheduleNext()
    }
  }

  await Promise.all(Array.from(Array(limit)).map(scheduleNext))

  return outputs
}


import { FileAccess } from './FileAccess'
import { FileType } from './FileType'

export interface FileInfo {
  name: string
  location: string
  type: FileType
  mimeType: string
  owner: string
  group: string
  mode: [[boolean, boolean, boolean], [boolean, boolean, boolean], [boolean, boolean, boolean]]
  ownerAccess: FileAccess
  groupAccess: FileAccess
  otherAccess: FileAccess
  created: Date
  modified: Date
  size: number
}

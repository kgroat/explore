
export enum FileType {
  file = 'FILE',
  executable = 'EXECUTABLE',
  directory = 'DIRECTORY',
}

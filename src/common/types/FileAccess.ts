
import { constants } from 'fs'

export enum FileAccess {
  readable = 1,
  writeable = 2,
  executable = 4,
}

export enum Accessor {
  owner = 0,
  group = 1,
  other = 2,
}

export enum AccessType {
  read = 0,
  write = 1,
  execute = 2,
}

export function translateMode (mode: number): [[boolean, boolean, boolean], [boolean, boolean, boolean], [boolean, boolean, boolean]] {
  return [
    [
      !!(mode & constants.S_IRUSR),
      !!(mode & constants.S_IWUSR),
      !!(mode & constants.S_IXUSR),
    ],
    [
      !!(mode & constants.S_IRGRP),
      !!(mode & constants.S_IWGRP),
      !!(mode & constants.S_IXGRP),
    ],
    [
      !!(mode & constants.S_IROTH),
      !!(mode & constants.S_IWOTH),
      !!(mode & constants.S_IXOTH),
    ],
  ]
}

export function translateFileAccess (access: [boolean, boolean, boolean]): FileAccess {
  const readAccess = access[AccessType.read] ? FileAccess.readable : 0
  const writeAccess = access[AccessType.write] ? FileAccess.writeable : 0
  const executeAccess = access[AccessType.execute] ? FileAccess.executable : 0

  return readAccess & writeAccess & executeAccess
}


require('ts-node').register({
  project: require.resolve('./tsconfig.webpack.json'),
})

const pcssFunctions = require('./src/renderer/pcss-functions/index.ts')

module.exports = {
  module: {
    rules: [
      {
        test: /\.p?css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
            },
          },
          'typed-css-modules-loader?noEmit',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => [
                require('postcss-import'),
                require('postcss-mixins'),
                require('postcss-each'),
                require('postcss-for'),
                require('postcss-simple-vars'),
                require('postcss-calc'),
                require('postcss-flexbugs-fixes'),
                require('postcss-functions')({
                  functions: pcssFunctions,
                }),
                require('postcss-color-function'),
                require('postcss-preset-env'),
                require('postcss-nested'),
              ],
            },
          },
          // 'resolve-url-loader',
        ],
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
        loader: 'file-loader'
      },
    ]
  }
}
